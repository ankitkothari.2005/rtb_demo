# tests/test_app.py

from app import app
import pytest

@pytest.fixture
def client():
    """Create a test client for the Flask application."""
    app.config['TESTING'] = True
    client = app.test_client()
    yield client

def test_hello(client):
    """Test the 'Hello, World!' route."""
    response = client.get('/')
    assert b'Hello' in response.data
