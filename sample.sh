#!/bin/bash

echo "The script name is: $0"
echo "All parameters passed to the script are:"

for arg in "$@"; do
  echo "$arg"
done
